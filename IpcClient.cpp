/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * This code is inspired from
 * https://github.com/monicaphalswal/unix-domain-socket-chat-client-server
 *
 * DFL::IPC::Client is unix domain socket based client, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Server to communicate between two instances of the same app.
 **/

#include "DFIpcClient.hpp"
#include "IpcClientImpl.hpp"

// C STL
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>

// Socket related
#include <sys/socket.h>
#include <sys/un.h>

// Other headers
#include <unistd.h>
#include <errno.h>

DFL::IPC::Client::Client( QString sockPath, QObject *parent ) : QObject( parent ) {
    mSockPath = sockPath;

    impl = new DFL::IPC::ClientImpl();
    impl->setAutoDelete( false );

    /** Ignore SIGPIPE */
    signal( SIGPIPE, SIG_IGN );
}


DFL::IPC::Client::~Client() {
    disconnectFromServer();

    impl->disconnect();
    delete impl;
}


bool DFL::IPC::Client::connectToServer() {
    /** We're connected to the server and have received a registration ID */
    if ( impl->mRegistered ) {
        return true;
    }

    /** SafetyNet: May be we're connected but yet to receive a registration ID */
    if ( impl->clientSocket.fd > 0 ) {
        return true;
    }

    /** Create socket: Local socket, with two way byte-stream connection */
    mSockFD = socket( AF_LOCAL, SOCK_STREAM, 0 );

    /** We failed to create a socket */
    if ( mSockFD == -1 ) {
        qCritical( "Failed to create a socket: %s", strerror( errno ) );
        emit socketError( errno );

        return false;
    }

    struct sockaddr_un server;

    /** Connect @mSockFD to the address */
    server.sun_family = AF_LOCAL;
    strncpy( server.sun_path, mSockPath.toUtf8().constData(), sizeof( server.sun_path ) - 1 );
    server.sun_path[ sizeof( server.sun_path ) - 1 ] = '\0'; // Ensure null-termination

    int ret = ::connect( mSockFD, (struct sockaddr *)&server, SUN_LEN( &server ) );

    if ( ret < 0 ) {
        qCritical( "Failed to connect to the server: %s", strerror( errno ) );

        close( mSockFD );
        mSockFD = -1;

        emit serverNotRunning();

        return false;
    }

    impl->clientSocket.fd     = mSockFD;
    impl->clientSocket.events = POLLRDNORM;

    connect( impl, &DFL::IPC::ClientImpl::messageReceived, this, &DFL::IPC::Client::messageReceived );
    connect( impl, &DFL::IPC::ClientImpl::connected,       this, &DFL::IPC::Client::connected );

    connect(
        impl, &DFL::IPC::ClientImpl::disconnected, [ = ] () {
            close( mSockFD );
            mSockFD = -1;

            emit disconnected();
        }
    );

    QThreadPool::globalInstance()->start( impl );

    return true;
}


bool DFL::IPC::Client::isConnected() {
    return ( impl && impl->mRegistered );
}


bool DFL::IPC::Client::waitForRegistered( qint64 timeout ) {
    qint64 time = 0;

    do {
        /** Low latency: Wait for 1 us */
        QThread::usleep( 1 );

        /** Bump the time count */
        time++;

        /** Don't let the UI thread hang */
        qApp->processEvents();

        /** If we've reached the time limit, quit the loop */
        if ( ( timeout > 0 ) and ( time >= timeout ) ) {
            return false;
        }
    } while ( not impl->mRegistered );

    /** We're registered */
    return true;
}


bool DFL::IPC::Client::sendMessage( QString msg ) {
    /** Write data into the sockfd */
    int bytes = write( mSockFD, msg.toUtf8().constData(), msg.size() );

    /** Flush the fd */
    fsync( mSockFD );

    /** We can now start waiting for the reply */
    impl->mReplied = false;
    impl->mReply   = QString();

    if ( msg.size() == bytes ) {
        return true;
    }

    qWarning() << "Error writing message:" << strerror( errno );

    return false;
}


bool DFL::IPC::Client::waitForReply( qint64 timeout ) {
    qint64 time = 0;

    do {
        /** Low latency: Wait for 1 us */
        QThread::usleep( 1 );

        /** Bump the time count */
        time++;

        /** Don't let the */
        qApp->processEvents();

        /** If we've reached the time limit, quit the loop */
        if ( ( timeout > 0 ) and ( time >= timeout ) ) {
            return false;
        }
    } while ( not impl->mReplied );

    /** We've received a reply. */
    return true;
}


QString DFL::IPC::Client::reply() {
    return impl->mReply;
}


void DFL::IPC::Client::disconnectFromServer() {
    if ( impl->mTerminate == false ) {
        impl->mTerminate = true;
        /** Force QApplication to clear it's queued events */
        qApp->processEvents();

        /** Wait for done makes sense only when started */
        if ( impl->mRegistered ) {
            /** 250 ms is plenty time to quit */
            bool ret = QThreadPool::globalInstance()->waitForDone( 250 );

            if ( ret == false ) {
                qWarning() << "Unable to stop polling.";
            }
        }
    }

    if ( mSockFD != -1 ) {
        close( mSockFD );
        mSockFD = -1;

        emit disconnected();
    }
}


bool DFL::IPC::Client::checkConnection( QString socketPath ) {
    if ( QFile::exists( socketPath ) == false ) {
        return false;
    }

    // Create a Unix domain socket
    int sockfd = socket( AF_UNIX, SOCK_STREAM, 0 );

    if ( sockfd == -1 ) {
        qDebug() << "Failed to create socket:" << strerror( errno );
        return false;
    }

    // Set up the socket address structure
    struct sockaddr_un addr;
    memset( &addr, 0, sizeof( addr ) );
    addr.sun_family = AF_UNIX;
    strncpy( addr.sun_path, socketPath.toUtf8().constData(), sizeof( addr.sun_path ) - 1 );

    // Attempt to connect to the server
    if ( ::connect( sockfd, reinterpret_cast<struct sockaddr *>( &addr ), sizeof( addr ) ) == -1 ) {
        qDebug() << "Failed to connect to server:" << strerror( errno );

        // Check if the socket file is stale (server crashed)
        if ( ( errno == ECONNREFUSED ) || ( errno == ENOENT ) ) {
            qDebug() << "Stale socket file detected. Removing:" << socketPath;

            if ( unlink( socketPath.toUtf8().constData() ) == -1 ) {
                qDebug() << "Failed to remove stale socket file:" << strerror( errno );
            }
        }

        close( sockfd );
        return false;
    }

    // Wait for a response from the server (with a timeout)
    struct pollfd pfd;
    pfd.fd     = sockfd;
    pfd.events = POLLIN; // Wait for data to read

    int timeout = 10;    // 10 ms timeout
    int nready  = poll( &pfd, 1, timeout );

    if ( nready == -1 ) {
        qDebug() << "Error while waiting for server response:" << strerror( errno );
        close( sockfd );
        return false;
    }

    else if ( nready == 0 ) {
        qDebug() << "Server did not respond within the timeout period.";
        close( sockfd );
        return false;
    }

    // Read the server's response
    char    response[ 256 ];
    ssize_t n = read( sockfd, response, sizeof( response ) - 1 );

    if ( n <= 0 ) {
        qDebug() << "Failed to read server response:" << strerror( errno );
        close( sockfd );
        return false;
    }

    // Null-terminate the response
    response[ n ] = '\0';

    qDebug() << "Server response:" << response;

    // Close the socket
    close( sockfd );

    qDebug() << "Server is running on: " << socketPath;

    /** The server is up and running */
    return true;
}
