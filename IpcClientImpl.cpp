/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * This code is inspired from
 * https://github.com/monicaphalswal/unix-domain-socket-chat-client-server
 *
 * DFL::IPC::Client is unix domain socket based client, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Server to communicate between two instances of the same app.
 **/

#include "IpcClientImpl.hpp"

// C STL
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>

// Socket related
#include <sys/socket.h>
#include <sys/un.h>

// Other headers
#include <unistd.h>
#include <errno.h>

DFL::IPC::ClientImpl::ClientImpl() : QRunnable() {
    clientSocket.fd = -1;
}


void DFL::IPC::ClientImpl::run() {
    while ( true ) {
        int nready = poll( &clientSocket, 1, 100 );
        Q_UNUSED( nready );

        if ( mTerminate ) {
            return;
        }

        if ( clientSocket.revents & ( POLLRDNORM | POLLERR ) ) {
            char message[ 2048 ] = { '\0' };

            memset( message, '\0', 2048 );
            int n = read( clientSocket.fd, message, 2048 );

            if ( n <= 0 ) {
                qCritical( "Server is offline.\n" );
                mTerminate  = true;
                mRegistered = false;
                emit disconnected();

                /** No point in waiting for events */
                return;
            }

            // read() returns -1 on error and sets errno
            if ( n <= 0 ) {
                if ( errno == EINTR ) {
                    // If the system call was interrupted by a signal before any data was
                    // available, then get more out of it.
                    continue;
                }

                else if ( ( errno == EAGAIN ) || ( errno == EWOULDBLOCK ) ) {
                    // No data to read from the socket at this time. Read later.
                    continue;
                }

                else {
                    qCritical( "Error reading from server: %d", errno );
                    qCritical( strerror( errno ) );
                    mTerminate  = true;
                    mRegistered = false;
                    emit disconnected();

                    /** No point in waiting for events */
                    return;
                }
            }

            if ( mRegistered == false ) {
                mRegistered = true;
                mClientID   = message;

                emit connected();
            }

            else if ( message == QString( "quit" ) ) {
                return;
            }

            else {
                mReply = message;
                emit messageReceived( message );
                mReplied = true;
            }
        }
    }
}
