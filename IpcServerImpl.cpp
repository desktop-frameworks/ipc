/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *
 * This code is inspired from
 * https://github.com/monicaphalswal/unix-domain-socket-chat-client-server
 *
 * DFL::IPC::Server is unix domain socket based server, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Client to communicate between two instances of the same app.
 **/

#include "IpcServerImpl.hpp"

// C STL
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>

// Socket related
#include <sys/socket.h>
#include <sys/un.h>

// Other headers
#include <unistd.h>
#include <errno.h>

#define MAX_CONNS    128

DFL::IPC::ServerImpl::ServerImpl( QObject *parent ) : QObject( parent ) {
    // Set auto-delete to false since we manage the object's lifetime manually
    setAutoDelete( false );
}


int DFL::IPC::ServerImpl::incomingConnection( int clientFD ) {
    int i;

    for ( i = 1; i < MAX_CONNS; i++ ) {
        if ( fdarr[ i ].fd < 0 ) {
            fdarr[ i ].fd     = clientFD;
            fdarr[ i ].events = POLLRDNORM;
            break;
        }
    }

    QString clientID = createUniqueID();

    lobby[ clientID ]        = clientFD;
    clientLookup[ clientFD ] = qMakePair( clientID, i );

    emit sendMessage( clientFD, clientLookup[ clientFD ].first );

    return i;
}


QString DFL::IPC::ServerImpl::createUniqueID() {
    QRandomGenerator *rand = QRandomGenerator::global();
    QString          id;

    char msg[ 9 ] = { '\0' };

    do {
        sprintf( msg, "0x%x", rand->bounded( 0x100000, 0xffffff ) );
    } while ( lobby.keys().contains( id ) );

    id = QString( msg );
    return id;
}


void DFL::IPC::ServerImpl::run() {
    int                listenfd, connfd;
    struct sockaddr_un client;
    char               message[ 2048 ] = { '\0' };

    int maxi = 0;

    int clilen = sizeof( struct sockaddr_un );

    while ( mTerminate == false ) {
        /**
         * Wait for one or more FD's to become ready to perform I/O.
         * Wait 100 ms in between operations.
         * QUESTION: Will this allow us to use signals and connections?
         */
        int nready = poll( fdarr, maxi + 1, 100 );

        /** Flush QApplication queue */
        qApp->processEvents();

        /** Some activity on @mSockFD: Incoming client connection. */
        if ( fdarr[ 0 ].revents & POLLRDNORM ) {
            connfd = accept( mSockFD, (struct sockaddr *)&client, (socklen_t *)&clilen );

            if ( connfd < 0 ) {
                qCritical() << "Failed to accept the incoming connection.";
            }

            /**
             * maxi is the current client count.
             * if @maxi == MAX_CONNS - 1, we've hit the client connection limit.
             */
            if ( maxi == MAX_CONNS - 1 ) {
                qWarning() << "Too many incoming connections";
                emit sendMessage( connfd, "Server is busy. Please try later." );
                close( connfd );
            }

            /** Okay.. We have space to connect to this client. */
            else {
                int idx = incomingConnection( connfd );

                /** Update the maximum number */
                maxi = std::max( maxi, idx );

                /** We've handled ONE of the available events */
                nready--;

                /**
                 * If there are no more events to handle,
                 * go back to waiting for more events.
                 */
                if ( nready <= 0 ) {
                    continue;
                }
            }
        }

        /** One or more of the client sockets became readable */
        for ( int i = 1; i <= maxi; i++ ) {
            if ( ( listenfd = fdarr[ i ].fd ) < 0 ) {
                continue;
            }

            if ( fdarr[ i ].revents & ( POLLRDNORM | POLLERR ) ) {
                memset( message, '\0', 2048 );
                int n = read( listenfd, message, 2048 );

                if ( n == 0 ) {
                    close( listenfd );
                    fdarr[ i ].fd = -1;

                    if ( clientLookup.contains( listenfd ) ) {
                        QString username = clientLookup[ listenfd ].first;
                        QString entry    = "[" + username + "] Terminated connection.";
                        clientLookup.remove( listenfd );
                        lobby.remove( username );
                        // QString response = lobbystatus();
                        // broadcast( response );
                    }
                }

                else if ( n < 0 ) {
                    if ( errno == ECONNRESET ) {
                        close( listenfd );
                        fdarr[ i ].fd = -1;
                    }

                    else {
                        // makeLog( 0, "read error" );
                    }
                }

                else {
                    emit messageReceived( message, fdarr[ i ].fd );
                }

                /** We've handled ONE of the available events */
                nready--;

                /**
                 * If there are no more events to handle,
                 * go back to waiting for more events, i.e.,
                 * break out of this clients loop.
                 */
                if ( nready <= 0 ) {
                    break;
                }
            }
        }
    }

    // Clean up resources
    for (int i = 0; i < MAX_CONNS; i++) {
        if ( fdarr[ i ].fd != -1 ) {
            close( fdarr[ i ].fd );
            fdarr[ i ].fd = -1;
        }
    }
}
