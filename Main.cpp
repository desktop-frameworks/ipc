/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * This code is inspired from
 * https://github.com/monicaphalswal/unix-domain-socket-chat-client-server
 *
 * DFL::IPC::Client is unix domain socket based client, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Server to communicate between two instances of the same app.
 **/

#include "DFIpcClient.hpp"
#include "Wayfire/DFWayfireIPC.hpp"
#include <iostream>
#include <string>

// int main( int argc, char *argv[] ) {
//     QCoreApplication app( argc, argv );

//     qRegisterMetaType<uint32_t>( "uint32_t" );

//     DFL::IPC::Wayfire *wf = new DFL::IPC::Wayfire( qgetenv( "WAYFIRE_SOCKET" ) );

//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::outputList, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::workspaceSetList, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::viewMapped, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::viewFocused, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::viewTitleChanged, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::viewOutputChanged, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::viewGeometryChanged, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::viewWorkspaceChanged, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::workspaceSetChanged, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::workspaceChanged, [=] ( nlohmann::json resp ) {
//     //         std::cout << resp.dump( 4 ) << std::endl;
//     //     }
//     // );
//     //
//     // QObject::connect(
//     //     wf, &DFL::IPC::Wayfire::genericEvent, [ = ] ( QJsonObject resp ) {
//     //         QString event = resp[ "event" ].toString();
//     //         qDebug() << event;

//     //         if ( event.startsWith( "view-" ) ) {
//     //             qDebug() << " " << resp[ "view" ].toObject()[ "app-id" ].toString();
//     //             // std::cout << resp[ "view" ].dump(4);
//     //         }

//     //         else {
//     //             std::cout << std::endl;
//     //         }

//     //         // if ( event == "view-set-output" ) {
//     //         //     std::cout << resp.dump( 4 ) << std::endl;
//     //         // }
//     //         //
//     //         // if ( event == "view-set-output" ) {
//     //         //     std::cout << resp.dump( 4 ) << std::endl << std::endl;
//     //         // }
//     //         //
//     //         // if ( event == "view-title-changed" ) {
//     //         //     std::cout << resp.dump( 4 ) << std::endl << std::endl;
//     //         // }
//     //         //
//     //         // if ( event == "view-app-id-changed" ) {
//     //         //     std::cout << resp.dump( 4 ) << std::endl << std::endl;
//     //         // }
//     //         //
//     //         // if ( event == "view-mapped" ) {
//     //         //     std::cout << resp.dump( 4 ) << std::endl << std::endl;
//     //         // }

//     //         if ( event == "view-focused" ) {
//     //             qDebug() << QJsonDocument( resp ).toJson().constData() << "\n";
//     //         }

//     //         // if ( event == "view-unmapped" ) {
//     //         //     std::cout << resp.dump( 4 ) << std::endl << std::endl;
//     //         // }
//     //     }
//     // );

//     wf->connectToServer();

//     QJsonArray test = wf->listOutputs();
//     qDebug() << test.count();
//     qDebug() << QJsonDocument( test ).toJson().data();

//     return app.exec();
// }

int main( int argc, char **argv ) {
    QCoreApplication app( argc, argv );
    qDebug() << "Attempting to contact DesQ Shell server at" << "/run/user/1000/DesQ/6/Shell.socket";

    if ( DFL::IPC::Client::checkConnection( "/run/user/1000/DesQ/6/Shell.socket" ) ) {
        qDebug() << "Connecting to server";
        DFL::IPC::Client shell( "/run/user/1000/DesQ/6/Shell.socket" );

        QObject::connect(
            &shell, &DFL::IPC::Client::connected, [] () {
                qDebug() << "Connected to server";
            }
        );

        QObject::connect(
            &shell, &DFL::IPC::Client::messageReceived, [ &app ] ( QString msg ) {
                qDebug() << "Message from server:" << msg;
                app.quit();
            }
        );

        qDebug() << shell.connectToServer();
        qDebug() << shell.waitForRegistered( 100 );

        if ( shell.isConnected() ) {
            qDebug() << "Sending message to DesQ Shell server: list-processes" << shell.sendMessage( "list-processes" );
            if ( shell.waitForReply( 1000 ) ) {
                qDebug() << shell.reply().toUtf8().constData();
            }
        }
    }

    return 0;
}
