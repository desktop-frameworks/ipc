/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * This code is inspired from
 * https://github.com/monicaphalswal/unix-domain-socket-chat-client-server
 *
 * DFL::IPC::Client is unix domain socket based client, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Server to communicate between two instances of the same app.
 **/

#pragma once

/** For struct pollfd */
#include <poll.h>

/* For QString, QThread, QTimer, etc.. */
#include <QtCore>

namespace DFL {
    namespace IPC {
        class ClientImpl;
    }
}

class DFL::IPC::ClientImpl : public QObject, public QRunnable {
    Q_OBJECT;

    public:
        ClientImpl();

        /** One FD for reading, one for writing */
        struct pollfd clientSocket;

        /** Unique ID for this client */
        QString mClientID;

        /** The last reply received */
        QString mReply;

        /** Registered */
        volatile bool mRegistered = false;

        /** Reply Received */
        volatile bool mReplied = false;

        /** Stop the loop flag */
        volatile bool mTerminate = false;

    protected:
        void run() override;

    Q_SIGNALS:
        /** Relay the message received from the server */
        void messageReceived( QString );

        /** Successfully connected to the server */
        void connected();

        /** Disconencted from the server */
        void disconnected();
};
