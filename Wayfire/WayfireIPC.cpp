/**
 * Copyright (c) 2025
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * DFL::IPC::Wayfire is the class to handle communication between Wayfire
 * and a client.
 **/

#include "DFWayfireIPC.hpp"
#include "WayfireImpl.hpp"

// Socket related
#include <sys/socket.h>
#include <sys/un.h>

// Other headers
#include <unistd.h>
#include <errno.h>
#include <bits/stdc++.h>


// Helper function to create a QJsonDocument from an initializer list
QJsonDocument createJsonObject( std::initializer_list<std::pair<QString, QJsonValue> > initList ) {
    QJsonObject obj;

    for (const auto& pair : initList) {
        obj.insert( pair.first, pair.second );
    }

    return QJsonDocument( obj );
}


DFL::IPC::Wayfire::Wayfire( QString wfSock ) : QObject() {
    impl             = new WayfireImpl();
    impl->wfSockPath = ( wfSock.isEmpty() ? qEnvironmentVariable( "WAYFIRE_SOCKET" ) : wfSock );

    /** Always emit this for any wayfire event */
    connect( impl, &DFL::IPC::WayfireImpl::wayfireEvent, this, &DFL::IPC::Wayfire::genericEvent );

    /** Parse the events and emit the correct signal */
    connect( impl, &DFL::IPC::WayfireImpl::wayfireEvent, this, &DFL::IPC::Wayfire::parseEvents );
}


DFL::IPC::Wayfire::~Wayfire() {
    impl->stop();

    if ( impl->wfSock.fd != -1 ) {
        close( impl->wfSock.fd );
        impl->wfSock.fd = -1;
    }

    delete impl;
    impl = nullptr;
}


bool DFL::IPC::Wayfire::connectToServer() {
    impl->wfSock.fd     = socket( AF_UNIX, SOCK_STREAM, 0 );
    impl->wfSock.events = POLLRDNORM;

    if ( impl->wfSock.fd == -1 ) {
        qCritical() << "Failed to create socket: " << strerror( errno );
        return false;
    }

    struct sockaddr_un addr;
    addr.sun_family = AF_UNIX,

    strncpy( addr.sun_path, impl->wfSockPath.toUtf8().data(), sizeof( addr.sun_path ) - 1 );
    addr.sun_path[ sizeof( addr.sun_path ) - 1 ] = '\0';


    if ( ::connect( impl->wfSock.fd, (struct sockaddr *)&addr, sizeof( addr ) ) == -1 ) {
        qCritical() << "Failed to connect to socket: " << strerror( errno );
        close( impl->wfSock.fd );
        return false;
    }

    impl->mConnected = true;

    /** Run the impl in a separate thread */
    QThreadPool::globalInstance()->start( impl );

    /** Make a request and forget about it. */
    QJsonObject request;
    request[ "method" ] = "window-rules/events/watch";
    genericRequest( QJsonDocument( request ) );

    return true;
}


QJsonArray DFL::IPC::Wayfire::listOutputs() {
    QJsonObject request;

    request[ "method" ] = "window-rules/list-outputs";

    QJsonDocument response = genericRequest( QJsonDocument( request ) );

    if ( response.isArray() ) {
        return response.array();
    }

    return QJsonArray();
}


bool DFL::IPC::Wayfire::focusOutput( int64_t ) {
    // QJsonObject request;
    // request[ "method" ] = "oswitch/switch-output";
    // request[ "data" ]   = {
    //     { "output-id", opId },
    // };
    //
    // QJsonDocument reply = genericRequest( QJsonDocument( request ) );
    //
    // if ( reply[ "result" ].toString() == "ok" ) {
    //     return true;
    // }

    return false;
}


bool DFL::IPC::Wayfire::showDesktop( int64_t opId ) {
    QJsonObject request;

    request[ "method" ] = "wm-actions/toggle_showdesktop";
    request[ "data" ]   = QJsonObject( {
                                           { "output_id", QJsonValue::fromVariant( (quint64)opId ) },
                                       } );

    QJsonDocument reply = genericRequest( QJsonDocument( request ) );

    return ( reply[ "result" ].toString() == "ok" );
}


QJsonDocument DFL::IPC::Wayfire::getWorkspaceSetsInfo() {
    QJsonDocument request = createJsonObject( {
                                                  { "method", "window-rules/list-wsets" },
                                              } );

    return genericRequest( QJsonDocument( request ) );
}


bool DFL::IPC::Wayfire::switchToWorkspace( int64_t opId, int64_t row, int64_t col ) {
    QJsonObject request;

    request[ "method" ] = "vswitch/set-workspace";
    request[ "data" ]   = QJsonObject( {
                                           { "output-id", QJsonValue::fromVariant( (quint64)opId ) },
                                           { "x", QJsonValue::fromVariant( (quint64)col ) },
                                           { "y", QJsonValue::fromVariant( (quint64)row ) },
                                       } );

    QJsonDocument reply = genericRequest( QJsonDocument( request ) );

    return ( reply[ "result" ].toString() == "ok" );
}


QJsonArray DFL::IPC::Wayfire::listViews() {
    QJsonDocument request = createJsonObject( {
                                                  { "method", "window-rules/list-views" },
                                              } );

    return genericRequest( request ).array();
}


QJsonObject DFL::IPC::Wayfire::getViewInfo( int64_t id ) {
    QJsonObject request;

    request[ "method" ] = "window-rules/view-info";
    request[ "data" ]   = QJsonObject( {
                                           { "id", QJsonValue::fromVariant( (quint64)id ) }
                                       } );

    QJsonObject reply = genericRequest( QJsonDocument( request ) ).object();

    if ( reply[ "result" ].toString() == "ok" ) {
        return reply[ "info" ].toObject();
    }

    return QJsonObject();
}


bool DFL::IPC::Wayfire::focusView( int64_t id ) {
    QJsonObject viewInfo = getViewInfo( id );

    if ( ( viewInfo.isEmpty() == false ) && ( viewInfo[ "minimized" ].toBool() == true ) ) {
        minimizeView( id, false );
    }

    QJsonObject request;
    request[ "method" ] = "window-rules/focus-view";
    request[ "data" ]   = QJsonObject( {
                                           { "id", QJsonValue::fromVariant( (quint64)id ) }
                                       } );

    QJsonObject reply = genericRequest( QJsonDocument( request ) ).object();

    return ( reply[ "result" ].toString() == "ok" );
}


bool DFL::IPC::Wayfire::minimizeView( int64_t id, bool yes ) {
    QJsonObject request;

    request[ "method" ] = "wm-actions/set-minimized";
    request[ "data" ]   = QJsonObject( {
                                           { "view_id", QJsonValue::fromVariant( (quint64)id ) }
                                       } );

    request[ "data" ] = QJsonObject( {
                                         { "state", yes }
                                     } );

    QJsonDocument reply = genericRequest( QJsonDocument( request ) );

    if ( reply[ "result" ] != "ok" ) {
        qDebug() << QJsonDocument( reply ).toJson().data() << "\n";
    }

    return ( reply[ "result" ].toString() == "ok" );
}


bool DFL::IPC::Wayfire::maximizeView( int64_t id, bool yes ) {
    QJsonObject request;

    if ( yes ) {
        request[ "method" ] = "grid/slot_c";
    }

    else {
        request[ "method" ] = "grid/restore";
    }

    request[ "data" ] = QJsonObject( {
                                         { "view-id", QJsonValue::fromVariant( (quint64)id ) }
                                     } );

    QJsonDocument reply = genericRequest( QJsonDocument( request ) );

    return ( reply[ "result" ].toString() == "ok" );
}


bool DFL::IPC::Wayfire::fullscreenView( int64_t id, bool yes ) {
    QJsonObject request;

    request[ "method" ] = "wm-actions/set-fullscreen";
    request[ "data" ]   = QJsonObject( {
                                           { "view_id", QJsonValue::fromVariant( (quint64)id ) },
                                           { "state", yes }
                                       } );

    QJsonDocument reply = genericRequest( QJsonDocument( request ) );

    return ( reply[ "result" ].toString() == "ok" );
}


bool DFL::IPC::Wayfire::restoreView( int64_t id ) {
    QJsonObject viewInfo = getViewInfo( id );

    if ( viewInfo.isEmpty() ) {
        return false;
    }

    /** If it's minimized, unminimize it */
    if ( viewInfo[ "minimized" ] == true ) {
        return minimizeView( id, false );
    }

    /** The view is maximized, unmaximize it */
    else if ( viewInfo[ "fullscreen" ] == true ) {
        return fullscreenView( id, false );
    }

    /** The view is maximized, unmaximize it */
    else if ( viewInfo[ "tiled-edges" ] == 15 ) {
        return maximizeView( id, false );
    }

    return false;
}


bool DFL::IPC::Wayfire::closeView( int64_t id ) {
    QJsonObject request;

    request[ "method" ] = "window-rules/close-view";
    request[ "data" ]   = QJsonObject( {
                                           { "id", QJsonValue::fromVariant( (quint64)id ) }
                                       } );

    QJsonDocument reply = genericRequest( QJsonDocument( request ) );

    return ( reply[ "result" ].toString() == "ok" );
}


QJsonDocument DFL::IPC::Wayfire::genericRequest( QJsonDocument request ) {
    if ( !impl->mConnected ) {
        QJsonDocument reply {
            { "result", "failed" }
        };
        return reply;
    }

    uint32_t reqId = impl->request( request );
    std::shared_ptr<QJsonDocument> reply = std::make_shared<QJsonDocument>();

    QEventLoop loop;

    auto connection = connect(
        impl, &DFL::IPC::WayfireImpl::response, &loop,
        [ &reply, reqId, &loop ](uint32_t id, QJsonDocument response) {
            if ( id == reqId ) {
                *reply = response;  // Update the content of the shared_ptr
                loop.quit();
            }
        }
    );

    loop.exec();

    // Disconnect the signal-slot connection to avoid any potential issues
    disconnect( connection );

    // Return the QJsonDocument, not the shared_ptr
    return *reply;
}


void DFL::IPC::Wayfire::parseEvents( QJsonDocument response ) {
    QString event = response[ "event" ].toString();

    if ( event == "view-mapped" ) {
        emit viewMapped( response );
    }

    else if ( event == "view-focused" ) {
        emit viewFocused( response );
    }

    else if ( event == "view-title-changed" ) {
        emit viewTitleChanged( response );
    }

    else if ( event == "view-app-id-changed" ) {
        emit viewAppIdChanged( response );
    }

    else if ( event == "view-geometry-changed" ) {
        emit viewGeometryChanged( response );
    }

    else if ( event == "view-tiled" ) {
        emit viewTitleChanged( response );
    }

    else if ( event == "view-minimized" ) {
        emit viewMinimized( response );
    }

    else if ( event == "view-set-output" ) {
        emit viewOutputChanged( response );
    }

    else if ( event == "view-workspace-changed" ) {
        emit viewWorkspaceChanged( response );
    }

    else if ( event == "view-unmapped" ) {
        emit viewUnmapped( response );
    }

    else if ( event == "output-added" ) {
        emit outputAdded( response );
    }

    else if ( event == "output-removed" ) {
        emit outputRemoved( response );
    }

    else if ( event == "output-gain-focus" ) {
        emit outputFocused( response );
    }

    else if ( event == "output-wset-changed" ) {
        emit workspaceSetChanged( response );
    }

    else if ( event == "wset-workspace-changed" ) {
        emit workspaceChanged( response );
    }

    else {
        emit genericEvent( response );
    }
}
