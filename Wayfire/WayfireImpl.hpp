/**
 * Copyright (c) 2025
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This code is based on socket-example.cpp from Wayfire.
 * https://github.com/WayfireWM/wayfire
 *
 * DFL::IPC::Client is unix domain socket based client, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Server to communicate between two instances of the same app.
 **/

#pragma once

/** For struct pollfd */
#include <poll.h>

/* For QString, QThread, QTimer, etc.. */
#include <QtCore>

namespace DFL {
    namespace IPC {
        class WayfireImpl;
    }
}

class DFL::IPC::WayfireImpl : public QObject, public QRunnable {
    Q_OBJECT;

    public:
        WayfireImpl();

        /** Stop polling
         * This will terminate the thread and delete this object
         */
        void stop();

        /** Request the compositor something */
        uint32_t request( QJsonDocument req );

        /** One FD for reading, one for writing */
        struct pollfd wfSock;

        /** The socket address */
        QString wfSockPath;

        /** Stop the loop flag */
        volatile bool mTerminate = false;

        /** Flag to check if we're connected or not */
        volatile bool mConnected = false;

        /** Function to write the json to wayfire socket. */
        bool writeJson( QJsonDocument j );

        /** Function to read the data from wayfire socket and parse it into json */
        QJsonDocument readJson();

    private:
        QList<uint> mPendingRequests;
        QMutex mutex; // Add a mutex for thread safety

        /** Function to read the json from wayfire socket into a buffer. */
        bool readExact( char *buf, uint size );

    protected:
        void run() override;

    Q_SIGNALS:
        /** We recieved an event from Wayfire */
        void wayfireEvent( QJsonDocument );

        /** Relay the message received from the server */
        void response( uint32_t, QJsonDocument );

        /** Inform the rest that polling has started */
        void started();
};
