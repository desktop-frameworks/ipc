/**
 * Copyright (c) 2025
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This code is based on socket-example.cpp from Wayfire.
 * https://github.com/WayfireWM/wayfire
 *
 * DFL::IPC::Client is unix domain socket based client, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Server to communicate between two instances of the same app.
 **/

#include "WayfireImpl.hpp"

// C STL
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cstdint>

// Socket related
#include <sys/socket.h>
#include <sys/un.h>

// Other headers
#include <unistd.h>
#include <errno.h>
#include <bits/stdc++.h>

DFL::IPC::WayfireImpl::WayfireImpl() : QRunnable() {
    wfSock.fd = -1;
}


void DFL::IPC::WayfireImpl::stop() {
    mTerminate = true;

    if ( wfSock.fd != -1 ) {
        close( wfSock.fd );
        wfSock.fd = -1;
    }

    qApp->processEvents();
}


uint32_t DFL::IPC::WayfireImpl::request( QJsonDocument req ) {
    QMutexLocker locker( &mutex );

    if ( !mConnected ) {
        return 0;
    }

    writeJson( req );

    // Generate a unique request ID
    static std::atomic<uint32_t> requestCounter( 0 );
    uint32_t reqId = ++requestCounter;

    if ( requestCounter == UINT32_MAX ) {
        requestCounter = 1;
    }

    mPendingRequests << reqId;
    return reqId;
}


void DFL::IPC::WayfireImpl::run() {
    emit started();

    while ( true ) {
        int nready = poll( &wfSock, 1, 10 );

        /** Something went wrong while polling */
        if ( nready < 0 ) {
            qWarning() << "[Error]:" << strerror( errno );
        }

        /** Bye bye..! */
        if ( mTerminate ) {
            return;
        }

        /** Nothing to read */
        if ( nready == 0 ) {
            continue;
        }

        /** We have something to read. Let's see what it is. */
        if ( wfSock.revents & ( POLLRDNORM | POLLERR ) ) {
            QJsonDocument resp = readJson();

            /** This is an event */
            if ( resp.isObject() && resp.object().contains( "event" ) ) {
                emit wayfireEvent( resp );
            }

            /** This is the response to a request */
            else {
                // Lock the mutex
                QMutexLocker locker( &mutex );

                uint32_t reqId = mPendingRequests.takeFirst();

                // Emit signal asynchronously
                QMetaObject::invokeMethod(
                    this, [ this, reqId, resp ]() {
                        emit response( reqId, resp );
                    },
                    Qt::QueuedConnection
                );

                emit response( reqId, resp );
            }
        }
    }
}


bool DFL::IPC::WayfireImpl::writeJson( QJsonDocument j ) {
    QByteArray str  = j.toJson( QJsonDocument::Compact );
    uint32_t   size = str.size();

    // Write the size of the JSON data
    ssize_t ret = write( wfSock.fd, &size, sizeof( size ) );

    if ( ret == -1 ) {
        // Handle write error
        qDebug() << "Failed to write size to socket:" << strerror( errno );
        throw std::runtime_error( "Failed to write size to socket" );
    }

    else if ( ret != sizeof( size ) ) {
        // Handle partial write
        qDebug() << "Partial write of size to socket:" << ret << "bytes written, expected" << sizeof( size );
        throw std::runtime_error( "Partial write of size to socket" );
    }

    // Write the JSON data
    const char *data        = str.constData();
    ssize_t    bytesWritten = 0;

    while ( bytesWritten < str.size() ) {
        ret = write( wfSock.fd, data + bytesWritten, str.size() - bytesWritten );

        if ( ret == -1 ) {
            // Handle write error
            qDebug() << "Failed to write JSON data to socket:" << strerror( errno );
            return false;
        }

        else if ( ret == 0 ) {
            // Handle socket closed by peer
            qDebug() << "Socket closed by peer while writing JSON data";
            return false;
        }

        bytesWritten += ret;
    }

    /** We succeeded in writing the JSON data successfully */
    return true;
}


QJsonDocument DFL::IPC::WayfireImpl::readJson() {
    uint32_t msgSize;

    if ( !readExact( (char *)&msgSize, sizeof( msgSize ) ) ) {
        return QJsonDocument();
    }

    std::vector<char> buf( msgSize );

    if ( !readExact( buf.data(), msgSize ) ) {
        return QJsonDocument();
    }

    return QJsonDocument::fromJson( QByteArray( buf.data(), msgSize ) );
}


bool DFL::IPC::WayfireImpl::readExact( char *buf, uint size ) {
    while ( size > 0 ) {
        int ret = read( wfSock.fd, buf, size );

        if ( ret == -1 ) {
            qCritical() << "Failed to read from socket: " << strerror( errno );
            return false;
        }

        buf  += ret;
        size -= ret;
    }

    return true;
}
