#include <nlohmann/json.hpp>
#include <cstdlib>
#include <iostream>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <ctime>

void write_json( int fd, nlohmann::json j ) {
    auto str = j.dump();

    uint32_t size = str.size();

    write( fd, &size,       sizeof( size ) );
    write( fd, str.c_str(), str.size() );
}


void read_exact( char *buf, int fd, uint32_t size ) {
    while ( size > 0 ) {
        int ret = read( fd, buf, size );

        if ( ret == -1 ) {
            std::cerr << "Failed to read from socket: " << strerror( errno ) << std::endl;
            exit( 1 );
        }

        buf  += ret;
        size -= ret;
    }
}


nlohmann::json read_json( int fd ) {
    uint32_t msg_size;

    read_exact( (char *)&msg_size, fd, sizeof( msg_size ) );

    char *buf = new char[ msg_size ];
    read_exact( buf,               fd, msg_size );
    nlohmann::json j = nlohmann::json::parse( buf, buf + msg_size );
    delete[] buf;
    return j;
}


int main() {
    char *sock = getenv( "WAYFIRE_SOCKET" );

    if ( !sock ) {
        std::cout << "Socket not found!" << std::endl;
        return 1;
    }

    // Open unix socket at address
    int fd = socket( AF_UNIX, SOCK_STREAM, 0 );

    if ( fd == -1 ) {
        std::cerr << "Failed to create socket: " << strerror( errno ) << std::endl;
        return 1;
    }

    struct sockaddr_un addr = {
        .sun_family = AF_UNIX,
    };
    strncpy( addr.sun_path, sock, sizeof( addr.sun_path ) - 1 );

    if ( connect( fd, (struct sockaddr *)&addr, sizeof( addr ) ) == -1 ) {
        std::cerr << "Failed to connect to socket: " << strerror( errno ) << std::endl;
        close( fd );
        return 1;
    }

    // nlohmann::json list_views;
    // list_views["method"] = "window-rules/list-views";
    // list_views["data"] = {};
    //
    // write_json(fd, list_views);
    // nlohmann::json response = read_json(fd);
    // std::cout << response.dump(2) << std::endl;
    //
    // std::cout << std::endl << std::endl;
    //
    // nlohmann::json list_wsets;
    // list_wsets[ "method" ] = "window-rules/view-info";
    // list_wsets[ "data" ]   = {
    //     { "id", 911 },
    // };
    //
    // write_json( fd, list_wsets );
    // nlohmann::json view_resp = read_json( fd );
    // std::cout << view_resp.dump( 4 ) << std::endl << std::endl;

    // nlohmann::json toggleDesk {
    //     { "method", "wm-actions/toggle_showdesktop" ,}
    // };
    //
    // write_json( fd, toggleDesk );
    // sleep( 2 );
    // write_json( fd, toggleDesk );

    nlohmann::json watch;
    watch[ "method" ] = "window-rules/events/watch";

    // watch[ "data" ] = {
    //     {"one", 1},
    //     {"one.one", 1.1},
    // };
    //
    write_json( fd, watch );

    std::cout << "Waiting for events" << std::endl;

    while ( true ) {
        /** Now we watch */
        nlohmann::json response = read_json( fd );

        if ( response.contains( "event" ) ) {
            // std::cout << response.dump( 4 ) << std::endl << std::endl;

            std::cout << response[ "event" ] << std::endl;
            // std::cout << std::endl;

            if ( response[ "event" ] == "view-workspace-changed" ) {
                std::cout << response.dump( 4 ) << std::endl;
                // for (auto& element : j.items()) {
                //     std::cout << element.key() << std::endl;
                // }
            }

            // if ( response[ "event" ] == "wset-workspace-changed" ) {
            //     std::cout << response.dump( 4 ) << std::endl;
            //     for (auto& element : j.items()) {
            //         std::cout << element.key() << std::endl;
            //     }
            // }

            if ( response[ "event" ] == "view-app-id-changed" ) {
                std::cout << response.dump( 4 ) << std::endl;
            }
        }
    }

    // nlohmann::json wsets;
    // wsets[ "method" ] = "window-rules/list-wsets";
    // write_json( fd, wsets );
    //
    // nlohmann::json response = read_json(fd);
    // std::cout << response.dump( 4 ) << std::endl;

    // nlohmann::json wsets;
    // wsets[ "method" ] = "window-rules/wset-info";
    // wsets[ "data" ]   = { { "id", 2 } };
    // write_json( fd, wsets );
    //
    // nlohmann::json response = read_json( fd );
    // std::cout << response.dump( 4 ) << std::endl;

    return 0;
}
