/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *
 * This code is inspired from
 * https://github.com/monicaphalswal/unix-domain-socket-chat-client-server
 *
 * DFL::IPC::Server is unix domain socket based server, wrapped in Qt/C++
 * class for convenience of use. This class is used in conjunction with
 * DFL::IPC:Client to communicate between two instances of the same app.
 **/

#pragma once

/** For struct pollfd */
#include <poll.h>

/* For QString, QThread, QTimer, etc.. */
#include <QtCore>

namespace DFL {
    namespace IPC {
        class ServerImpl;
    }
}

class DFL::IPC::ServerImpl : public QObject, public QRunnable {
    Q_OBJECT;

    public:

        /**
         * @path is the socket path.
         */
        ServerImpl( QObject *parent = nullptr );

        /**
         * Handle an incoming connection
         * This will have a unique fd, which will be watched for messages.
         */
        int incomingConnection( int clientFD );

        /**
         * Handle a register request
         * Reply with the Uinque ID
         */
        void registerConnection( int clientFD );

        /**
         * Create unique ID;
         */
        QString createUniqueID();

        /** Socket FD */
        int mSockFD = -1;

        /** Lobby */
        QMap<QString, int> lobby;

        /** Lookup */
        QMap<int, QPair<QString, int> > clientLookup;

        /** Array for polling */
        struct pollfd fdarr[ 128 ];

        /** Terminate flag */
        volatile bool mTerminate = false;

    protected:
        void run() override;

    Q_SIGNALS:
        /* Relay the commands */
        void messageReceived( QString, int );

        /** Disconencted from the server */
        void disconnected();

        /** Send message to the client */
        void sendMessage( int, QString );
};
